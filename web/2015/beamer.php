<div class="page-header">
  <h1 id="beamer">Präsentieren mit Beamer</h1>
</div>

<p class="lead">Präsentieren mit LaTeX statt mit PowerPoint.</p>

<p>Die Beamer-Klasse ermöglicht es, mit LaTeX schnell und einfach Folien für Präsentationen zu erstellen. In diesem Vortrag wollen wir in die Verwendung dieses Pakets einführen.</p>

<p>Wir besprechen den Aufbau von LaTeX-Dokumenten, die zu Präsentation kompiliert werden können, und die verschiedenen Layouts, die die Beamer-Klasse zur Verfügung stellt. Weiter erläutern wir wie Gliederungen eingeblendet werden können, mit welchen Befehlen Elemente auf den Folien strukturiert werden können und schließlich wie die Elemente schrittweise ein- und ausgeblendet werden können.</p>

<p>Ein großer Vorteil bei der Verwendung der Beamer-Klasse liegt darin, dass aus dem gleichen LaTeX-Dokument die Folien und ein begleitender Artikel für einen Vortrag generiert werden kann. Wir werden an einem kurzen Beispiel das Zusammenspiel der einzelnen Komponenten dafür vorführen.</p>

<p><strong>Voraussetungen:</strong> Es werden grundlegende Kenntnisse von LaTeX vorausgesetzt, wie sie zum Beispiel im Grundlagen-Vortrag vermittelt wurden.</p>

<ul>
  <li>Was ist Beamer?
    <ul>
      <li>Einleitung</li>
      <li>Eigenschaften</li>
    </ul>
  </li>
  <li>Verwendung von Beamer
    <ul>
      <li>Folien</li>
      <li>Strukturelemente</li>
      <li>Form</li>
    </ul>
  </li>
  <li>Fortgeschrittene Verwendung
    <ul>
      <li>Overlays</li>
      <li>Artikelfassung</li>
    </ul>
  </li>
</ul>

<h2 id="beamer-folien">Folien</h2>

<p>Die Folien des Vortrags können hier im PDF-Format heruntergeladen werden. Der Quelltext der Vorträge selber befindet sich bei <a href="https://github.com/malteschmitz/latex">Github</a>.</p>

<p>
  <a href="slides-beamer.pdf" class="btn btn-lg btn-primary"><span class="glyphicon glyphicon-download"></span> Folien herunterladen</a>
</p>

<h2 id="beamer-beispiele">Beispieldokumente</h2>

<ul>
  <li>[<a href="examples/beamer.tex">TEX</a>] [<a href="examples/beamer.pdf">PDF</a>] Präsentation voller Beispiele</li>
  <li>[<a href="examples/overlays.tex">TEX</a>] [<a href="examples/overlays.pdf">PDF</a>] Overlays</li>
  <li>[<a href="examples/sandhaufen.content.tex">TEX</a>] Inhalt des Sandhaufen-Vortrags</li>
  <li>[<a href="examples/sandhaufen.beamer.tex">TEX</a>] [<a href="examples/sandhaufen.beamer.pdf">PDF</a>] Folienfassung des Sandhaufen-Vortrags</li>
  <li>[<a href="examples/sandhaufen.article.tex">TEX</a>] [<a href="examples/sandhaufen.article.pdf">PDF</a>] Artikelfassung des Sandhaufen-Vortrags</li>
</ul>

<h2 id="beamer-literatur">Zum Weiterlesen</h2>

<p>Till Tantau, Joseph Wright und Vedran Miletić.<br>
The <span style="font-variant:small-caps">beamer</span> <em>class</em>,<br>
<a href="http://mirrors.ctan.org/macros/latex/contrib/beamer/doc/beameruserguide.pdf"><tt>beameruserguide.pdf</tt></a>, Oktober 2013.</p>

<p>Till Tantau.<br>
<em>Beamer: Strahlende Vorträge mit LaTeX</em>,<br>
Präsentieren und Dokumentieren &ndash; Tools.<br>
Vorlesung vom 31. Oktober 2012.</p>

<p>Sebastian Pipping.<br>
The <span style="font-variant:small-caps">beamer</span> Theme Matrix,<br>
<a href="http://www.hartwork.org/beamer-theme-matrix/"><tt>hartwork.org/beamer-theme-matrix</tt></a>, April 2009.</p>