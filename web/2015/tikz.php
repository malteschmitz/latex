<div class="page-header">
  <h1 id="tikz">Zeichnen mit Ti<em>k</em>Z</h1>
</div>

<p class="lead">Zeichnen geht auch ohne Zeichenprogramm.</p>

<p>Das TeX-Paket Ti<em>k</em>Z ermöglicht es, in LaTeX Zeichnungen zu setzen. Wir wollen in diesem Vortrag aufzeigen, wie simple Zeichnungen, aber auch Graphen, Bäume und Mind-Maps durch wenige einfache Befehle direkt aus einem LaTeX-Dokument heraus gezeichnet werden können. </p>

<p>Warum will man das? Weil schnell professionelle Zeichnungen entstehen. Weil die Zeichnungen perfekt zum restlichen LaTeX-Dokument passen, da sie das Schriftbild des umgebenden Dokuments übernehmen. Und weil es Spaß macht.</p>

<p><strong>Voraussetungen:</strong> Für diesen Vortrag sind nur sehr grundlegende Erfahrungen mit LaTeX nötig, da Ti<em>k</em>Z eine relativ eigenständige Syntax hat.</p>

<ul>
  <li>Einführung
    <ul>
      <li>Verwendung</li>
      <li>Pfade</li>
      <li>Knoten</li>
    </ul>
  </li>
  <li>Graphen
    <ul>
      <li>Knoten</li>
      <li>Automaten</li>
      <li>Bäume</li>
    </ul>
  </li>
  <li>Fortgeschrittene Verwendung
    <ul>
      <li>Funktionen plotten</li>
      <li>Overlays mit Beamer</li>
      <li>Showcase</li>
    </ul>
  </li>
</ul>

<h2 id="tikz-folien">Folien</h2>

<p>Die Folien des Vortrags können hier im PDF-Format heruntergeladen werden. Der Quelltext der Vorträge selber befindet sich bei <a href="https://github.com/malteschmitz/latex">Github</a>.</p>

<p>
  <a href="slides-tikz.pdf" class="btn btn-lg btn-primary"><span class="glyphicon glyphicon-download"></span> Folien herunterladen</a>
</p>

<h2 id="tikz-beispiele">Beispieldokumente</h2>

<ul>
  <li>[<a href="examples/pfade.tex">TEX</a>] [<a href="examples/pfade.pdf">PDF</a>] Pfade, Koordinaten und Schnittpunkte</li>
  <li>[<a href="examples/knoten.tex">TEX</a>] [<a href="examples/knoten.pdf">PDF</a>] Knoten</li>
  <li>[<a href="examples/automaten.tex">TEX</a>] [<a href="examples/automaten.pdf">PDF</a>] Automaten</li>
  <li>[<a href="examples/baum.tex">TEX</a>] [<a href="examples/baum.pdf">PDF</a>] Bäume</li>
  <li>[<a href="examples/funktionen.tex">TEX</a>] [<a href="examples/funktionen.pdf">PDF</a>] Funktionen plotten</li>
  <li>[<a href="examples/tikz-overlays.tex">TEX</a>] [<a href="examples/tikz-overlays.pdf">PDF</a>] Overlays mit Beamer</li>
</ul>

<h2 id="tikz-literatur">Zum Weiterlesen</h2>

<p>Till Tantau.<br>
The Ti<em>k</em>Z and <span style="font-variant:small-caps">pgf</span> Packages,<br>
Manual for version 2.10,<br>
<a href="http://mirrors.ctan.org/graphics/pgf/base/doc/generic/pgf/pgfmanual.pdf"><tt>pgfmanual.pdf</tt></a>, Oktober 2010.</p>

<p>Kjell Magne Fauske und Stefan Kottwitz.<br>
<em>TeXample.net</em>,<br>
ample resources for TeX users,<br>
<a href="http://www.texample.net/tikz/examples/"><tt>texample.net</tt></a>.</p>