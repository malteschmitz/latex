<div class="page-header">
  <h1 id="nook">MetaNook</h1>
</div>

<p class="lead">MetaMeute engages the awesome Night of Open Knowledge.</p>

<p>Die Night of Open Knowledge (Nook) der MetaMeute in Lübeck ist eine offene Vortragsveranstaltung, die aus einem LaTeX-Einführungsvortrag hervorgegangen ist. Auf der <a href="http://www.metameute.de/nook">Nook 2015</a> übernehmen wir zum dritten Mal die LaTeX-Vorträge.</p>

<p>Auf dieser Seite finden sich alle Informationen zu den Vorträgen: Die Folien, die Skriptfassung, Beispieldokumente zum direkt loslegen und Links zum Weiterlesen. Von <a href="../2013">2013 existieren auch <em>Audio- und Videoaufzeichnungen</em></a>.</p>