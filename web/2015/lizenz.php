<div class="page-header">
  <h1 id="lizenz">Lizenz</h1>
</div>

<p class="lead">Free as in speech not just free as in beer.</p>

<h2 id="lizenz-quelltext">Quelltext</h2>

<p>Der Quelltext der Vorträge, der Beispiele und dieser
  <a href="http://www.mlte.de/latex">Website</a>
  sind auf <a href="http://github.com/malteschmitz/latex">GitHub</a>
  unter <a href="http://www.opensource.org/licenses/MIT">MIT-Lizenz</a>
  veröffentlicht.</p>

<p>Die Beispiele aus <em>The TikZ and PGF Packages</em> und von <em>TeXample.net</em>
  unterliegen den dort angegebenen freien Lizenzen.</p>

<p>The MIT License</p>

<p>Copyright (c) 2013 Johannes Thorn, Malte Schmitz</p>

<p>Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:</p>

<p>The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.</p>

<p>THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.</p>